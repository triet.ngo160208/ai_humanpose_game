import math
import cv2 as cv
import random 
from RandomPoint import RandomPoint

class Pattern:
    
    def __init__(self):
        self.first_pattern = [RandomPoint(450, 300), RandomPoint(300, 300), RandomPoint(200, 200), RandomPoint(350, 350), RandomPoint(300, 300), RandomPoint(350, 350), RandomPoint(350, 350), RandomPoint(350, 300)]
        self.second_pattern = [RandomPoint(550, 300), RandomPoint(400, 400), RandomPoint(350, 350), RandomPoint(650, 350), RandomPoint(500, 300), RandomPoint(550, 350), RandomPoint(550, 350), RandomPoint(350, 400)]
        self.third_pattern = [RandomPoint(350, 400), RandomPoint(600, 300), RandomPoint(450, 450), RandomPoint(350, 450), RandomPoint(300, 500), RandomPoint(450, 450), RandomPoint(450, 450), RandomPoint(550, 400)]
        self.fourth_pattern = [RandomPoint(650, 400), RandomPoint(700, 400), RandomPoint(650, 300), RandomPoint(650, 450), RandomPoint(500, 450), RandomPoint(650, 450), RandomPoint(450, 250), RandomPoint(550, 500)]

        self.getPattern(0)

    def random(self):
        level = random.randrange(0, 6, 1)
        self.getPattern(level)
        return self.ListPoints

    def getPattern(self, index):
        self.ListPoints = []

        first_point = self.first_pattern[index]
        second_point = self.second_pattern[index]
        third_point = self.third_pattern[index]
        fourth_point = self.fourth_pattern[index]

        first_point.state = False
        second_point.state = False
        third_point.state = False
        fourth_point.state = False

        self.ListPoints.append(first_point)
        self.ListPoints.append(second_point)
        self.ListPoints.append(third_point)
        self.ListPoints.append(fourth_point)


   

       
       

    
    
   