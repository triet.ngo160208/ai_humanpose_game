import math
import cv2 as cv
import random 

class RandomPoint:
    
    def __init__(self, x, y, color=(0,0,255)):
        self.x = x
        self.y = y
        self.color = color
        self.state = False

    def distance(self, other):
        dx = self.x - other[0]
        dy = self.y - other[1]
        return math.sqrt(dx**2 + dy**2)
    
    def draw(self, frame):
        cv.circle(frame, (self.x, self.y), 35, self.color,  thickness= -1)
        return frame

    def checkPointIn(self, centers):
        for center in centers:
            if 55 >= self.distance(center):
                return True
        return False

    def random(self):
        self.x = self.x + random.randrange(50, 150, 50)
        self.y = self.y + random.randrange(50, 150, 50)

   