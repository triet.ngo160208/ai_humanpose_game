import cv2
import argparse
import numpy as np
import time
import argparse
import imutils
from utils import choose_run_mode, load_pretrain_model, set_video_writer
from imutils.video import FPS
from Pose.pose_visualizer import TfPoseVisualizer
from Pose.coco_format import CocoPart, CocoColors, CocoPairsRender
from VideoShow import *
from HumanPose import *
import random 
import math
from VideoGet import VideoGet
# import the necessary packages
# from __future__ import print_function
# from imutils.video import WebcamVideoStream
# from imutils.video import FPS
# import argparse
# import imutils
# from VideoShow import *
# import cv2
# created a *threaded* video stream, allow the camera sensor to warmup,
# and start the FPS counter
print("[INFO] sampling THREADED frames from webcam...")



estimator = load_pretrain_model('VGG_origin')

video_getter = VideoGet(0).start()
video_shower = VideoShow(video_getter.frame).start()
human_pose = HumanPose(video_getter.frame).start()

def ShowHumanPose(frame):
    # human_pose.frame = frame
    # humans = human_pose.humans
    humans = estimator.inference(frame)
    height, width = frame.shape[:2]
    if humans != None:
        for human in humans:
            for i in range(CocoPart.Background.value):
                if i in human.body_parts.keys():

                    body_part = human.body_parts[i]
                    center_x = body_part.x * width + 0.5
                    center_y = body_part.y * height + 0.5
                    center = (int(center_x), int(center_y))
                    print(center, 'center')
                    cv2.circle(frame, center, 20, (0, 255, 0), thickness= -1, lineType=20, shift=0) 
    return frame


# loop over some frames...this time using the threaded stream
while True:
    if video_getter.stopped or video_shower.stopped:
            video_shower.stop()
            video_getter.stop()
            break

    frame = video_getter.frame
    frame = ShowHumanPose(frame)
    video_shower.frame = frame
    
    # frame = putIterationsPerSec(frame, cps.countsPerSec())
   
    

    
    # video_shower.frame = frame
    # cps.increment()

  

# stop the timer and display FPS information


# do a bit of cleanup

