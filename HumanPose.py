from threading import Thread
import cv2
import imutils
from utils import choose_run_mode, load_pretrain_model, set_video_writer
from imutils.video import FPS
from Pose.pose_visualizer import TfPoseVisualizer
from Pose.coco_format import CocoPart, CocoColors, CocoPairsRender
import time

estimator = load_pretrain_model('VGG_origin')

class HumanPose:
    """
    Class that continuously shows a frame using a dedicated thread.
    """

    def __init__(self, frame=None, humans=None):
        self.frame = frame
        self.stopped = False
        self.humans = humans

    def start(self):
        Thread(target=self.show, args=()).start()
        return self

    def show(self):
        while not self.stopped:
            self.humans = estimator.inference(self.frame)
            # time.sleep(1)
                

    def stop(self):
        self.stopped = True