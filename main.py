import cv2 as cv
import argparse
import numpy as np
import time
import argparse
import imutils
from utils import choose_run_mode, load_pretrain_model, set_video_writer
from imutils.video import FPS
from Pose.pose_visualizer import TfPoseVisualizer
from Pose.coco_format import CocoPart, CocoColors, CocoPairsRender
from HumanPose import *
import random 
import math
import threading
from RandomPoint import RandomPoint
from Pattern import Pattern

cap = cv.VideoCapture(0)
level = 0
mark = 0
center_x = 0
center_y = 0
centers = []

pattern = Pattern()
ListPoints = pattern.ListPoints

(grabbed, frame) = cap.read()

old_humans = None

human_pose = HumanPose(frame).start()
while cv.waitKey(1) < 0:

    status = True
    for point in ListPoints:
        if point.state == False:
            status = False

    if status:
        status = False
        mark += 5
        ListPoints = pattern.random()
    

    (grabbed, frame) = cap.read()
    if not grabbed or human_pose.stopped:
        human_pose.stop()
        break
    frame = imutils.resize(frame, width=1100, height=650)
    frame = cv2.flip(frame, 1)
    overlay = frame.copy()
    
    # frame = putIterationsPerSec(frame, cps.countsPerSec())
    human_pose.frame = frame
    humans = human_pose.humans
    height, width = frame.shape[:2]

    # humans = estimator.inference(frame)
    humans = human_pose.humans
    
    # pose = TfPoseVisualizer.draw_pose_rgb(frame, humans)
    height, width = frame.shape[:2]

    for i in range(1,10):
        if humans != None:
            for human in humans:
                for i in range(CocoPart.Background.value):
                    if i in human.body_parts.keys():
                        body_part = human.body_parts[i]
                        center_x = body_part.x * width + 0.5
                        center_y = body_part.y * height + 0.5
                        center = (int(center_x), int(center_y))
                
                        cv.circle(frame, center, 10, (0, 255, 0), thickness= -1,
                                lineType=20, shift=0)
        if old_humans != None:
            for human in old_humans:
                for i in range(CocoPart.Background.value):
                    if i in human.body_parts.keys():
                        body_part = human.body_parts[i]
                        center_x = body_part.x * width + 0.5
                        center_y = body_part.y * height + 0.5
                        center = (int(center_x), int(center_y))
                        centers.append(center)
                        cv.circle(frame, center, 10, (0, 255, 0), thickness= -1,
                                lineType=20, shift=0)

    
    for point in ListPoints:
        if point.checkPointIn(centers):
            point.color = (0,0,255)
            
        else:
            point.color = (255,0,0)

                
        frame = point.draw(frame)

    for i in range(0, len(ListPoints)):
        if ListPoints[i].checkPointIn(centers):
            ListPoints[i].state = True
        else:
            ListPoints[i].state = False

    if (old_humans != humans):
        old_humans = humans

    centers = []
    opacity = 0.4
    showPoint = 'Point: ' + str(mark)
    cv2.addWeighted(overlay, opacity, frame, 1 - opacity, 0, frame)
    cv2.putText(frame, showPoint, (30, 90), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 2, cv2.LINE_AA)
    cv2.imshow("Video", frame)
    
cap.release()
cv2.destroyAllWindows()
